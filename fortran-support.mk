# Support code that may be used in debian/rules
# Alastair McKinstry <mckinstry@debian.org>
# 2024-01-04

include /usr/share/dpkg/buildflags.mk
include /usr/share/dpkg/architecture.mk

# WARNING: THIS CODE IS EXPERIMENTAL AND IN HIGH FLUX

# This will set:
#    FC_DEFAULT : the default compiler flavor
#    FC_OPTIONAL : a space seperated list of compiler flavors present except FC_DEFAULT

# FC_DEFAULT is /etc/alternatives/f95 unless FC is specified in the environment

# FC flavors: shortnames for Fortran compilers, currently:
#  gfortran flang flangext lfortran flang18 flangext18 flang19 flangext19


ifeq ($(origin F77),default)
  F77:=$(shell basename $(shell readlink /etc/alternatives/f77))
endif
ifeq ($(origin FC),environment)
  FC_DEFAULT:=$(FC)
endif
ifeq ($(origin FC),default)
  FC_DEFAULT:=$(shell basename $(shell readlink /etc/alternatives/f95))
endif

FC_COMPILERS:= $(if $(wildcard /usr/bin/gfortran-13), gfortran , ) \
	       $(if $(wildcard /usr/bin/gfortran-14), gfortran , )  \
	       $(if $(wildcard /usr/bin/gfortran-15), gfortran , )  \
	       $(if $(wildcard /usr/bin/flang-new-17), flang , )  \
	       $(if $(wildcard /usr/bin/flang-new-18), flang18 , )  \
	       $(if $(wildcard /usr/bin/flang-new-19), flang19 , )  \
	       $(if $(wildcard /usr/bin/flang-new-20), flang20 , )  \
	       $(if $(wildcard /usr/bin/flang-to-external-fc-17), flangext , )  \
	       $(if $(wildcard /usr/bin/flang-to-external-fc-18), flangext18 , )  \
	       $(if $(wildcard /usr/bin/lfortran), lfortran, )  

FC_OPTIONAL:=$(filter-out $(FC_DEFAULT),$(FC_COMPILERS))

# Functions: take flavor as parameter

# get_fmoddir:
#   return the Module(s)directory usable by this flavour

fort_root = /usr/lib/${DEB_HOST_MULTIARCH}/fortran

# Currently gfortran defaults to gfortran-14, flang to flang-19

# Install in this directory
get_fmoddir = $(strip \
	       $(if $(filter $1,gfortran), ${fort_root}/gfortran-mod-15, \
	      $(if $(filter $1, flang), ${fort_root}/flang-mod-15, \
	      $(if $(filter $1, flang17), ${fort_root}/flang-mod-15, \
	      $(if $(filter $1, flang18), ${fort_root}/flang-mod-15, \
	      $(if $(filter $1, flang19), ${fort_root}/flang-mod-15, \
	      $(if $(filter $1, flang20), ${fort_root}/flang-mod-15, \
	      $(if $(filter $1, flangext), ${fort_root}/flangext-mod-15, \
	      $(if $(filter $1, flangext17), ${fort_root}/flangext-mod-15, \
	      $(if $(filter $1, flangext18), ${fort_root}/flangext-mod-15, \
	      $(if $(filter $1, lfortran), ${fort_root}/lfortran-mod-0, \
					   ${fort_root}/$1 )))))))))))

# Compatible modules for include search. Directories may not be present
get_fmoddirs = $(if $(filter $1,gfortran),  -I${fort_root}/gfortran-mod-15 -I${fort_root}/flangext-mod-15, \
	       $(if $(filter $1, flang),    -I${fort_root}/flang-mod-15, \
	       $(if $(filter $1, flang17),    -I${fort_root}/flang-mod-15, \
	       $(if $(filter $1, flang18),    -I${fort_root}/flang-mod-15, \
	       $(if $(filter $1, flang19),    -I${fort_root}/flang-mod-15, \
	       $(if $(filter $1, flang20),    -I${fort_root}/flang-mod-15, \
	       $(if $(filter $1, flangext), -I${fort_root}/flangext-mod-15 -I${fort_root}/gfortran-mod-15, \
	       $(if $(filter $1, flangext18), -I${fort_root}/flangext-mod-15 -I${fort_root}/gfortran-mod-15, \
	       $(if $(filter $1, lfortran), -I${fort_root}/lfortran-mod-0, \
					    -I${fort_root}/$1 )))))))))

# get_fc_exe
#   Compiler name, suitable for -DCMAKE_Fortran_COMPILER=$(call get_fc_exe,XXX)

get_fc_exe = $(strip \
	     $(if $(filter $1,gfortran), /usr/bin/gfortran-15, \
	     $(if $(filter $1,gfortran), /usr/bin/gfortran-14, \
	     $(if $(filter $1,gfortran), /usr/bin/gfortran-13, \
	     $(if $(filter $1,flang),    /usr/bin/flang-new-17, \
	     $(if $(filter $1,flang18),    /usr/bin/flang-new-18, \
	     $(if $(filter $1,flang19),    /usr/bin/flang-new-19, \
	     $(if $(filter $1,flang20),    /usr/bin/flang-new-20, \
	     $(if $(filter $1,flangext), /usr/bin/flang-to-external-fc-17, \
	     $(if $(filter $1,flangext18), /usr/bin/flang-to-external-fc-18, \
	     $(if $(filter $1,lfortran), /usr/bin/lfortran, \
					 /usr/bin/$(FC_DEFAULT)  )))))))))))

# Inverse of get_fc_exe
#

# TODO  get_fc_flavor = $(if $(filter $(shell $1,/usr/bin//usr/bin/gfortran), gfortran, FALSE)

get_flibdir=/usr/lib/${DEB_HOST_MULTIARCH}/fortran/$1

# Compatible directories for library search. Directories may not be present
get_flibdirs = $(if $(filter $1,gfortran), -L${fort_root}/gfortran -L${fort_root}/flangext, \
	       $(if $(filter $1,flang),    -L${fort_root}/flang, \
	       $(if $(filter $1,flang18),  -L${fort_root}/flang, \
	       $(if $(filter $1,flang19),  -L${fort_root}/flang, \
	       $(if $(filter $1,flang20),  -L${fort_root}/flang, \
	       $(if $(filter $1,flangext), -L${fort_root}/flangext -L${fort_root}/gfortran, \
	       $(if $(filter $1,flangext18), -L${fort_root}/flangext18 -L${fort_root}/gfortran, \
	       $(if $(filter $1,lfortran), -L${fort_root}/lfortran, \
					   -L${fort_root}/$1 ))))))))



# Compiler-specific fixes to FCFLAGS

# See #957692

FCLAGS_ADD_GFORTRAN10:= -fallow-invalid-boz -fallow-argument-mismatch
FCLAGS_ADD_GFORTRAN11:= -fallow-invalid-boz -fallow-argument-mismatch
FCLAGS_ADD_GFORTRAN12:= -fallow-invalid-boz -fallow-argument-mismatch
FCLAGS_ADD_GFORTRAN13:= -fallow-invalid-boz -fallow-argument-mismatch
FCLAGS_ADD_GFORTRAN14:= -fallow-invalid-boz -fallow-argument-mismatch

FCFLAGS_FILTER_FLANG7:= -g 
FCFLAGS_FILTER_FLANG17:= -mbranch-protection=standard -fstack-protector-strong -fstack-clash-protection

FCFLAGS_ADD_gfortran:= $(FCLAGS_ADD_GFORTRAN13)
FCFLAGS_ADD_flangext:= $(FCLAGS_ADD_GFORTRAN13)
FCFLAGS_FILTER_flang:= $(FCFLAGS_FILTER_FLANG17)


### Support code for configure/build.
### Assuming dh_X_fortran_TEMPLATE is defined, use that to build / configure
### Must succeed on FC_DEFAULT, may fail on FC_OPTIONAL compilers
### (for capturing details in logs )

# Potentially add others if $FC is set (eg ifx, Arm)

# TODO: improve on "-f debian/rules"
define dh_fortran_RECURSE =
 dh_$(1)_fortran_$(2)_nofail:
	@echo DEBUGF RECURSE into dh_$(1)_fortran_$(2)
	@($(MAKE) -f debian/rules dh_$(1)_fortran_$(2) || echo " FAILURE IGNORED")
	@echo DEBUGF LEAVING dh_$(1)_fortran_$(2)_nofail
endef

# This will iterate over targets and recursively call make but to always succeed
define foreach_fc_dot =
  $(foreach f, $(FC_DEFAULT) $(FC_OPTIONAL), $(eval $(call dh_$(1)_fortran_TEMPLATE,$(f))))
  $(foreach f, $(FC_OPTIONAL) , $(eval $(call dh_fortran_RECURSE,$(1),$(f))))
endef

define foreach_fc_execute_before =
  $(eval $(call foreach_fc_dot,$(1)))
  execute_before_dh_auto_$(1): $(foreach f, $(FC_DEFAULT), dh_$(1)_fortran_$(f)) $(foreach f, $(FC_OPTIONAL), dh_$(1)_fortran_$(f)_nofail)
endef

define foreach_fc_execute_after =
  $(eval $(call foreach_fc_dot,$(1)))
  execute_after_dh_auto_$(1): $(foreach f, $(FC_DEFAULT), dh_$(1)_fortran_$(f)) $(foreach f, $(FC_OPTIONAL), dh_$(1)_fortran_$(f)_nofail)
endef

override_dh_dwz:
	@echo "DWZ currently breaks because of patchelf, used in dh_fortran_lib"
